package ca.csf.mobile1.tp1;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by mthb5 on 2017-02-27.
 */
public class ConsoleView {


    public interface Listener{
        void onDataEntered(String input);
    }

    private ArrayList<Listener> listeners;

    public ConsoleView()
    {
        listeners = new ArrayList<Listener>();
    }

    public void addListener(Listener listener)
    {
        listeners.add(listener);
    }

    public void writeToConsole(String output)
    {
        System.out.println(output);
    }

    public void readFromConsole()
    {
        Scanner userInput = new Scanner(System.in);
        String line  = userInput.nextLine();

        for( Listener listener : listeners)
        {
            listener.onDataEntered(line);
        }
    }

    public void show()
    {
        System.out.println("Entrez une formule :");
        this.readFromConsole();
    }

}
