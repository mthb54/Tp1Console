package ca.csf.mobile1.tp1;

import ca.csf.mobile1.tp1.chemical.compound.ChemicalCompound;
import ca.csf.mobile1.tp1.chemical.compound.ChemicalCompoundFactory;

/**
 * Created by mthb5 on 2017-02-27.
 */
public class Controller implements ConsoleView.Listener {

    private  ConsoleView view;
    private  ChemicalCompoundFactory chemicalFactory;


    public Controller(ConsoleView view , ChemicalCompoundFactory chemicalFactory)
    {
        this.view = view;
        this.view.addListener(this);
        this.chemicalFactory = chemicalFactory;
    }


    public void onDataEntered(String input)
    {
        ChemicalCompound compound;
        try
        {
            compound = chemicalFactory.createFromString(input);
            view.writeToConsole(Double.toString(compound.getWeight()) +" g/mol");
        }
        catch (Exception e)
        {
            view.writeToConsole(e.getMessage());
        }
    }

    /**
     * Affiche l'interface de début.
     */
    public void Show()
    {
        this.view.show();
    }
}
