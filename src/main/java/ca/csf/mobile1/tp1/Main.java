package ca.csf.mobile1.tp1;

import ca.csf.mobile1.tp1.chemical.compound.ChemicalCompoundFactory;
import ca.csf.mobile1.tp1.chemical.element.ChemicalElement;
import ca.csf.mobile1.tp1.chemical.element.ChemicalElementRepository;
import com.sun.deploy.uitoolkit.ui.ConsoleController;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws Exception {
        //TODO Créer les objets du modèle
        //TODO Créer la vue
        //TODO Créer le controleur


        ChemicalElementRepository elementRepository = new ChemicalElementRepository();
        ChemicalCompoundFactory chemicalFactory ;
        ConsoleView view = new ConsoleView();
        Controller controller;


        String line;
        InputStream inputStream = ClassLoader.getSystemResourceAsStream("chemicalElements.txt");
        BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream));

        while((line = bReader.readLine())!= null)
        {
            int counter =0;
            String temp = "";
            String name = "";
            String symbol ="";
            int number = 0;
            double weight = 0;
            // create new chemical elements with each line in text file. 44 = coma
            for (int i =0; i< line.length(); i++)
            {
                if ( line.charAt(i) != 44)
                {
                    temp += line.charAt(i);
                }
                else if(line.charAt(i) == 44)
                {
                    if(counter ==0)
                    {
                        name += temp;
                        temp = "";
                    }
                    else if(counter == 1)
                    {
                        symbol += temp;
                        temp = "";
                    }
                    else if(counter == 2)
                    {
                        number += Integer.parseInt(temp);
                        temp = "";
                    }
                    counter++;
                }
            }
            weight = Double.parseDouble(temp);
            ChemicalElement element = new ChemicalElement(name ,symbol ,number , weight);
            elementRepository.add(element);
        }
        bReader.close();
        inputStream.close();

        chemicalFactory = new ChemicalCompoundFactory(elementRepository);
        controller = new Controller(view , chemicalFactory);

        controller.Show();
    }



}


